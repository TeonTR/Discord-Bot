package TeonTR;

import me.Teoman00.TMain;
import me.Zamion101.main.ZTeon;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

import javax.security.auth.login.LoginException;

public class TeonBot {

    public static JDABuilder JDA;

    public static void main(String[] args){
        JDABuilder jda = new JDABuilder(AccountType.BOT).setToken(Vars.token);
        jda.setAutoReconnect(true);
        Game g = Game.streaming("TeonTR - https://www.discord.me/teon","https://www.discord.me/teon");
        jda.setGame(g);
        JDA = jda;
        ZTeon.init();
        TMain.enable(jda);
        try {
            jda.buildBlocking();
        } catch (LoginException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RateLimitedException e) {
            e.printStackTrace();
        }
    }
}
