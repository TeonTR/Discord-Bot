package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.CommandSearch;
import me.Zamion101.manager.TCommand;
import me.Zamion101.manager.TInterface;
import net.dv8tion.jda.core.entities.Message;

import java.util.Iterator;
import java.util.List;

public class CCommand extends TCommand implements TInterface{

    @Override
    public void run(Information info, TMessage message) {
        super.run(info,message);
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        for (Iterator<String> it = CommandSearch.commandClass.keySet().iterator(); it.hasNext(); ) {
            String c = it.next();
            System.out.println(c + " / " + CommandSearch.commandPerm.get(c) + " / " + CommandSearch.isAdmin(info.user));
            if(CommandSearch.commandPerm.get(c) == 0){
                sb.append("-te!" + c + "\n");
            }else if(CommandSearch.commandPerm.get(c) == 1 && CommandSearch.isAdmin(info.user)){
                sb.append("Admin -te!" + c + "\n");
            }else{
                continue;
            }

        }
        sb.append("```");
        info.user.openPrivateChannel().complete().sendMessage(info.user.getAsMention() + " TeonTR sunucusunda kullanabileceğiniz komutlar;\n"
                + sb.toString()).queue();
    }

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("komutlar",this.getClass(),0);
    }

}
