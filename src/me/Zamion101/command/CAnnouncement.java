package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.EmailChecker;
import me.Zamion101.manager.TCommand;
import me.Zamion101.socket.EDuyuru;
import me.Zamion101.socket.Email;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

public class CAnnouncement extends TCommand{


    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("duyuru", this.getClass(), 1);
    }


    @Override
    public void run(Information info, TMessage message) {
        super.run(info, message);
        StringBuilder sb = new StringBuilder();
        for(String s : message.getArgs()){
            sb.append(s + " ");
        }
        String m = sb.toString();
        for(Member member : info.guild.getMembers()){
            User user = member.getUser();
            if(EmailChecker.emailRegistered(user)){
                String to = EmailChecker.getEmail(user);
                String d = "<html><h1 style=\"text-align: center;\"><span style=\"color: #339966;\"><strong>TeonTR</strong></span></h1>\n" +
                        "<h2 style=\"text-align: center;\"><span style=\"color: #000000;\">Duyuru - @" + info.user.getName() + "</span></h2>\n" +
                        "<p style=\"text-align: left;\"><strong>Sevgili &uuml;yemiz<span style=\"color: #ff0000;\"> " + user.getName() +"</span>,</strong></p>\n" +
                        "<p style=\"text-align: left;\"><strong>@" + info.user.getName() + " size bir duyuru mesajı g&ouml;nderdi mesajın i&ccedil;eriği aşağıda yazmaktadır;</strong></p>\n" +
                        "<p style=\"text-align: left;\"><strong>" + m +"</strong></p>\n" +
                        "<p style=\"text-align: right;\"><strong>İyi Eğlenceler.</strong></p>\n" +
                        "<p>&nbsp;</p>\n" +
                        "<p>&nbsp;</p></html>";

                new EDuyuru(info,to,d);
            }
        }
    }
}
