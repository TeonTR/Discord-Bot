package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;
import me.Zamion101.manager.UserFinder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

public class CBan extends TCommand{

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("ban", this.getClass(), 1);
    }

    @Override
    public void run(Information info, TMessage message) {
        super.run(info,message);
        String[] args = message.getArgs();
        StringBuilder sb = new StringBuilder();
        for(int a = 1; a < args.length; a++){
            sb.append(args[a] + " ");
        }
        String reason = sb.toString();
        User user = message.user[0];
        info.messageChannel.sendMessage("" + user.getAsMention() + " (B A N L A N D I N!) Sebep: " + reason).queue();
    }
}
