package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.listener.ReadyListener;
import me.Zamion101.manager.CommandSearch;
import me.Zamion101.manager.TCommand;
import me.Zamion101.manager.TInterface;
import net.dv8tion.jda.core.entities.Guild;

public class CReload extends TCommand implements TInterface{
    @Override
    public void run(Information info, TMessage message) {
        super.run(info,message);
        try{
            ReadyListener.admins.clear();
            ReadyListener.searchAdmin(info.event);
            info.messageChannel.sendMessage("Başarıyla yenileme işlemi tamamlandı.").queue();
        }catch (Exception e){
            info.messageChannel.sendMessage("Yenileme sırasında hata meydana geldi lütfen logları konrol ediniz!").queue();
        }
    }

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("yenile",this.getClass(),1);
    }
}
