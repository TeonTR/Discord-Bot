package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;

public class CReport extends TCommand{


    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("rapor", this.getClass(), 0);
    }

    @Override
    public void run(Information info, TMessage message) {
        super.run(info, message);
        String[] args = message.getArgs();
        if(args.length > 1){
            switch (args[0].toLowerCase()){
                case "küfür":
                    if(args.length > 2){
                        info.messageChannel.sendMessage("" + info.user.getAsMention() + " Lütfen sadece 1 adet küfür yazınız!").queue();
                    }else{
                        info.messageChannel.sendMessage(""+ info.user.getAsMention() + " Raporunuz için teşekkür ederiz. Raporunuzun Yapay Zeka tarafından incelenmesi için işleme alınmıştır. İyi eğlenceler.").queue();
                    }
                    break;
                case "hata":
                    if(args.length < 2){
                        info.messageChannel.sendMessage("" + info.user.getAsMention() + " te!rapor <Hata> <Mesaj>").queue();
                    }else{
                        StringBuilder sb = new StringBuilder();
                        for(int a = 1; a < args.length; a++){
                            sb.append(args[a] + " ");
                        }
                        String mesaj = sb.toString();
                        System.out.println(sb);
                    }
                    break;
                default:
                    info.messageChannel.sendMessage("" + info.user.getAsMention() + "t e!rapor <Küfür/Hata> <Mesaj>").queue();
                    break;
            }
        }else{
            info.messageChannel.sendMessage(""+ info.user.getAsMention() + " te!rapor <Durum> <Mesaj>").queue();
        }
    }
}
