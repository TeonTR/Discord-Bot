package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.main.ZTeon;
import me.Zamion101.manager.TCommand;
import me.Zamion101.socket.Email;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class CEposta extends TCommand{


    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("eposta", this.getClass(), 0);
    }


    @Override
    public void run(Information info, TMessage message) {
        super.run(info, message);
        PreparedStatement pd;
        try {
            pd = ZTeon.con.prepareStatement("SELECT * FROM register WHERE user_id = ?");
            pd.setString(1,info.user.getId());
            ResultSet rs = pd.executeQuery();
            rs.next();
            if(rs.getString("eposta") == null || rs.getString("eposta").isEmpty() || rs.getString("eposta").equalsIgnoreCase( " ")){
                if(message.getArgs().length > 1){
                    info.messageChannel.sendMessage(info.user.getAsMention() + " Komutun kullanımı şu şekilde; te!eposta <eposta adresin>").queue();
                }else{

                    PreparedStatement ps;
                    try {
                        ps = ZTeon.con.prepareStatement("SELECT * FROM register WHERE user_id = ?");
                        ps.setString(1,info.user.getId());
                        ResultSet rd = ps.executeQuery();
                        if(rd.next()){
                            String code = UUID.randomUUID().toString();
                            ps = ZTeon.con.prepareStatement("INSERT INTO email (user_id,email,code) VALUES (?,?,?)");
                            ps.setString(1,info.user.getId());
                            ps.setString(2,message.getArgs()[0]);
                            ps.setString(3,code);
                            ps.execute();
                            info.privateChannel.sendMessage("Başarılı bir şekilde doğrulama linki gönderildi!" +
                                    " Lütfen size gönderilen e-postadaki linke tıklayarak doğrulama işlemini tamamlayınız.").queue();
                            new Email(info,message.getArgs()[0],code);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                info.messageChannel.sendMessage(info.user.getAsMention() + " Zaten e-posta adresin sistemde kayıtlı. Değiştirmek için lütfen '@Zamion101' ile iletişime geç. İyi eğlenceler.").queue();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
