package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;

public class CHelp extends TCommand{

    @Override
    public void run(Information info, TMessage message) {
        super.run(info,message);
        info.user.openPrivateChannel().complete().sendMessage("Demek yardım istedin koçum fakat şuan ben bile" +
                " ne olduğumu bilmiyorum daha sonra tekrar uğradığında sana ne bildiğimi söylerim..." +
                " Şimdilik iyi günler.").queue();
    }

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("yardım", this.getClass(), 0);
    }
}
