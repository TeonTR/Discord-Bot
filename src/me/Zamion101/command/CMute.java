package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;
import me.Zamion101.manager.TRoles;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;


public class CMute extends TCommand{

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("mute", this.getClass(), 1);
    }

    @Override
    public void run(Information info, TMessage message) {
        super.run(info, message);
        User user = message.user[0];
        String[] args = message.getArgs();
        if(args.length > 2){
            StringBuilder sb = new StringBuilder();
            for(int a = 1; a < args.length; a++){
                sb.append(args[a] + " ");
            }
            String reason = sb.toString();
        }
        Member m = info.guild.getMember(user);
        info.guild.getController().modifyMemberRoles(m, TRoles.getRole(info, TRoles.HALK), TRoles.getRole(info,TRoles.SUSTURULMUS)).queue();
    }
}
