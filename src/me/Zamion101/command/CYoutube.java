package me.Zamion101.command;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;
import me.Zamion101.manager.TInterface;

public class CYoutube extends TCommand implements TInterface {

    @Override
    public void run(Information info, TMessage message) {
        super.run(info,message);
        info.user.openPrivateChannel().complete().sendMessage(info.user.getAsMention() + " Kanala abone olarak bizi" +
                " destekleyebilirsin. https://www.youtube.com/channel/UC8KdU8qxbhaPJSdYZrSAplA").queue();
    }

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("youtube",this.getClass(),0);
    }
}
