package me.Zamion101;

import me.Zamion101.manager.UserFinder;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TMessage {
    //TODO Boşluklu isimleri tek kişi olarak algılaması sağlanacak!
    //TODO Regx kullanarak isimleri algılama sağlanabilir.
    public String[] raw;
    public String[] args;
    public User[] user;

    public TMessage(Information info, String message){
        raw = message.split(" ");
        System.out.println(Arrays.toString(raw));
        List<String> r = new ArrayList<>();
        for(int a= 1; a < raw.length; a++){
            r.add(raw[a]);
        }
        args = new String[r.size()];
        args = r.toArray(args);
        r.clear();
        System.out.println(Arrays.toString(args));
        List<User> u = new ArrayList<>();
        for(String st: args){
            if(st.startsWith("@")){
                User user = UserFinder.userByName(info,st.substring(1));
                u.add(user);
                System.out.println("USER FOUNDED! " + user.getName());
            }
        }
        user = new User[u.size()];
        user = u.toArray(user);
        u.clear();
    }

    public String[] getArgs(){
        return args;
    }

    public User[] getUser(){
        return user;
    }

    public String getID(int id){
        return getUser()[id].getId();
    }
}
