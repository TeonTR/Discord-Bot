package me.Zamion101;

import jdk.nashorn.internal.objects.annotations.Getter;
import me.Zamion101.listener.ReadyListener;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.List;

public class Information<T extends Event> {

    public Guild guild;
    public MessageChannel messageChannel;
    public User user;
    public String content;
    public Message message;
    public MessageType messageType;
    public PrivateChannel privateChannel;
    public Event event;
    public List<Role> roles;

    public Information(T event){
        this.event = event;
        if(event instanceof MessageReceivedEvent){
            guild = ((MessageReceivedEvent)event).getGuild();
            messageChannel = ((MessageReceivedEvent)event).getChannel();
            user = ((MessageReceivedEvent)event).getAuthor();
            content = ((MessageReceivedEvent)event).getMessage().getContent();
            message = ((MessageReceivedEvent)event).getMessage();
            messageType = ((MessageReceivedEvent)event).getMessage().getType();
        }

        if(event instanceof GuildMemberRoleAddEvent){
            this.guild = ((GuildMemberRoleAddEvent)event).getGuild();
            this.user = ((GuildMemberRoleAddEvent)event).getUser();
            this.roles = ((GuildMemberRoleAddEvent)event).getRoles();
        }

        if(event instanceof GuildMemberRoleRemoveEvent){
            this.guild = ((GuildMemberRoleAddEvent)event).getGuild();
            this.user = ((GuildMemberRoleAddEvent)event).getUser();
            this.roles = ((GuildMemberRoleAddEvent)event).getRoles();
        }

        if(event instanceof Event){
            this.guild = event.getJDA().getGuildById(ReadyListener.id);
            this.roles = this.guild.getRoles();
        }

        if(user != null){
            privateChannel = user.openPrivateChannel().complete();
        }
    }

}
