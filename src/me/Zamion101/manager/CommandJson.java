package me.Zamion101.manager;


import TeonTR.Vars;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Iterator;

public class CommandJson {


    public FileReader input;
    public File file;

    public CommandJson(){
        file = new File(Vars.jsonPath);
        if(!file.exists()) try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            input = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        JSONParser parser = new JSONParser();
        JSONObject json = null;
        try {
            Object obj = parser.parse(input);
            json = (JSONObject)obj;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray array = (JSONArray)json.get("commands");

        Iterator<Object> iterator = array.iterator();
        while (iterator.hasNext()) {
            JSONObject ob = (JSONObject) iterator.next();
            try {
                try {
                    CommandSearch.registerCommand(new String(((String)(ob.get("name"))).getBytes("UTF-8"),"UTF-8"),Class.forName(((String)ob.get("class"))),(int)(long)ob.get("permission"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }


    }


}
