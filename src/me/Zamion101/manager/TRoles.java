package me.Zamion101.manager;

import me.Zamion101.Information;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Role;

import javax.transaction.TransactionRolledbackException;

public enum TRoles {
    TEON("Teon","393115911202013185"),SULTAN("Sultan","393468188412739584"),VEZIR("Vezir","393119234521300993"),HATUN("Hatun","393472313909182465"),KADI("Kadı","394848596811579406"),BEYLIK("Beylik","393116032249757699"),HALK("Halk","393408579836706828"),SUSTURULMUS("Susturulmuş","395319898596966400");

    public String name;
    public String id;

    TRoles(String name,String id){
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public TRoles[] roles(){
        return values();
    }

    public static Role getRole(Information info, TRoles role){
        return info.guild.getRoleById(role.id);
    }
}
