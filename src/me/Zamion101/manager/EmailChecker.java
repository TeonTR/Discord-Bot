package me.Zamion101.manager;

import me.Zamion101.main.ZTeon;
import net.dv8tion.jda.core.entities.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmailChecker {


    public static boolean emailRegistered(User user){
        PreparedStatement ps;
        try {
            ps = ZTeon.con.prepareStatement("SELECT * FROM register WHERE user_id = ?");
            ps.setString(1,user.getId());
            ResultSet rs = ps.executeQuery();
            if(rs.next() && rs.getString("eposta") !=null){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static String getEmail(User user){
        if(emailRegistered(user)){
            PreparedStatement ps;
            try {
                ps = ZTeon.con.prepareStatement("SELECT * FROM register WHERE user_id = ?");
                ps.setString(1,user.getId());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    return rs.getString("eposta");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
