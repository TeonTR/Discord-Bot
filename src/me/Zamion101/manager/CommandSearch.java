package me.Zamion101.manager;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.listener.ReadyListener;
import net.dv8tion.jda.core.entities.User;

import java.util.HashMap;

public class CommandSearch {

    public static HashMap<String,Class<? extends TCommand>> commandClass = new HashMap<>();


    public static HashMap<String,Integer> commandPerm = new HashMap<>();
    //PERMS => 0 = "Everyone", 1 = "Admin", 2 = "TEON!"


    public static boolean isValid(String command){
        return commandClass.containsKey(command.toLowerCase());
    }

    public static void runCommand(Information info, TMessage message, String command){
        if(isValid(command)){
            boolean c = false;
            switch (commandPerm.get(command)){
                case 0:
                    c = true;
                case 1:
                    if(ReadyListener.admins.contains(info.user.getId())) c = true;
                default:
                    if(c){
                        try {
                            commandClass.get(command.toLowerCase()).newInstance().run(info,message);
                            System.out.println("[R]: " + command + " / " + commandClass.get(command).getSimpleName());
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }else{
                        info.messageChannel.sendMessage(info.user.getAsMention() + " Kusura bakma fakat bu komutu kullanmak için yetkin bulunmamaktadır. İyi günler.").queue();
                    }
                    break;
            }

        }else{
            info.messageChannel.sendMessage(info.user.getAsMention() + "Kusura bakma fakat \"`te!" + command + "`\" şeklinde bir komut sistemde tanımlı değil! `te!yardım` yada `te!komutlar` yazarak tekrar kontrol edebilirisin.").queue();
        }
    }

    public static void registerCommand(String command,Class clazz,int permission){
        if(!isValid(command.toLowerCase())){
            commandClass.put(command.toLowerCase(),clazz);
            commandPerm.put(command.toLowerCase(),permission);
            System.out.println("[RC]: " + command + " / " + clazz + " / " + clazz.getSimpleName());
        }else{
            System.out.println("Command \"te!" + command.toLowerCase() + "\" already exist!");
        }
    }

    public static void register(Class<?extends TCommand>... clazz){
        for(Class<?extends TCommand> c : clazz){
            try {
                c.newInstance().init(null,null,0);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isAdmin(User user){
        return ReadyListener.admins.contains(user.getId());
    }


}
