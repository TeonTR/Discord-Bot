package me.Zamion101.manager;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import net.dv8tion.jda.core.entities.Guild;

public interface TInterface {

        void run(Information info, TMessage message);

        void init(String command, Class clazz, int permission);

}
