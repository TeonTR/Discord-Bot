package me.Zamion101.manager;

import me.Zamion101.Information;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

import java.util.Iterator;

public class UserFinder {

    public static User userByName(Information info ,String name){
        for (Iterator<Member> it = info.guild.getMembers().iterator(); it.hasNext(); ) {
            Member u = it.next();
            if(u.getUser().getName().equalsIgnoreCase(name)){
                return u.getUser();
            }else{
                continue;
            }
        }
        return null;
    }

    public static String idByName(Information info ,String name){
        return userByName(info,name).getId();
    }
}
