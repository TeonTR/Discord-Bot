package me.Zamion101.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseLogin {

    Connection con;

    String host;
    int port;
    String database;
    String username;
    String password;

    public DatabaseLogin(String ahost,int aport,String adatabase,String ausername,String apassword){
        this.host = ahost;
        this.port = aport;
        this.database = adatabase;
        this.username = ausername;
        this.password = apassword;
        connect();
    }

    public void connect(){
        if (this.con == null) {
            try {
                this.con = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.username + "&password=" +
                        this.password + "&autoReconnect=true");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void disconnect() {
        try {
            this.con.close();
            this.con = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return this.con;
    }


}
