package me.Zamion101.socket;

import me.Zamion101.Information;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class EDuyuru extends Thread{

    public EDuyuru(Information info, String to, String duyuru){
        final String username = "noreply.teontr@gmail.com";
        final String password = "******";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            try {
                message.setFrom(new InternetAddress("no-reply.TeonTR@gmail.com","TeonTR"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject("TeonTR - Duyuru @" + info.user.getName());
            message.setContent(duyuru, "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Duyuru Gönderildi!");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
