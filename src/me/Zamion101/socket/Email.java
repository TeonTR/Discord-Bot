package me.Zamion101.socket;

import me.Zamion101.Information;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class Email {

    public Email(Information info,String to,String code){
        final String username = "noreply.teontr@gmail.com";
        final String password = "berkem2604";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.addHeader("Content-type", "text/HTML; charset=UTF-8");
            message.addHeader("format", "flowed");
            message.addHeader("Content-Transfer-Encoding", "8bit");
            try {
                message.setFrom(new InternetAddress("no-reply.TeonTR@gmail.com","TeonTR"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("TeonTR Eposta Doğrulama");

            String s = "<html><h1 style=\"text-align: center;\"><span style=\"color: #339966;\"><strong>TeonTR</strong></span></h1>\n" +
                    "<p style=\"text-align: center;\"><span style=\"color: #000000;\"><strong>E-Posta Doğrulama</strong></span></p>\n" +
                    "<p style=\"text-align: left;\"><strong>Sevgili &uuml;yemiz<span style=\"color: #ff0000;\"> " + info.user.getName() + "</span>,</strong></p>\n" +
                    "<p style=\"text-align: left;\"><strong>E-Posta adresinizi onaylamak i&ccedil;in size bu e-postayı g&ouml;nderiyoruz. " +
                    "<a title=\"TeonTR - E-Posta Doğrulama\" href=\"www.teon.minestand.network/email.php?key=" + code + "&id=" + info.user.getId() + "\" target=\"_blank\" rel=\"noopener\">Buraya</a> tıklayarak e-posta adresinizi doğrulayabilirsiniz. </strong></p>\n" +
                    "<p style=\"text-align: right;\"><strong>İyi Eğlenceler.</strong></p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p>&nbsp;</p></html>";
            message.setContent(s, "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Eposta Gönderildi!");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


}
