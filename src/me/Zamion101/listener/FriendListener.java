package me.Zamion101.listener;

import net.dv8tion.jda.client.events.relationship.FriendAddedEvent;
import net.dv8tion.jda.client.events.relationship.FriendRequestReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class FriendListener extends ListenerAdapter{

    @Override
    public void onFriendRequestReceived(FriendRequestReceivedEvent event) {
        System.out.println("FRIEND REQUEST!");
        event.getFriendRequest().accept();
    }

    @Override
    public void onFriendAdded(FriendAddedEvent event) {
        System.out.println("FRIEND ADDED! " + event.getFriend().getUser().getName());
    }
}
