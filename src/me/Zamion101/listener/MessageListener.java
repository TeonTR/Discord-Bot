package me.Zamion101.listener;

import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.main.ZTeon;
import me.Zamion101.manager.CommandSearch;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MessageListener extends ListenerAdapter{

    public PreparedStatement ps;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if(!event.getAuthor().isBot() && (event.getChannel().getName().equalsIgnoreCase("sohbet-muhabbet") || event.getChannel().getName().equalsIgnoreCase("bot-test"))){
            Information info = new Information<MessageReceivedEvent>(event);

            try {
                ps = ZTeon.con.prepareStatement("SELECT * FROM register WHERE user_id = ?");
                ps.setString(1,info.user.getId());
                ResultSet rs = ps.executeQuery();
                if(!rs.next()){
                    ps = ZTeon.con.prepareStatement("INSERT INTO register (user,user_id) VALUES (?,?)");
                    ps.setString(1,info.user.getName());
                    ps.setString(2,info.user.getId());
                    ps.executeUpdate();
                    System.out.println("USER REGISTERED!");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


            System.out.println("[Log]- " + info.user.getName() + " -{ \" " + info.content + " \" }-");

            if(!ReadyListener.admins.contains(info.user.getId())){
                List<TextChannel> tc = info.guild.getTextChannels();
                for(TextChannel t : tc){
                    if(t.getName().equalsIgnoreCase("bot-log")){
                        t.sendMessage("[Log]- " + info.user.getAsMention() + " -{ \" " + info.content + " \" }-").queue();
                    }
                }
            }else{

            }

            if(info.content.startsWith("te!")){
                if(info.content.toLowerCase().equals("te!")){
                    info.messageChannel.sendMessage("Merhaba "+ info.user.getAsMention() + ", ben TeonTR! Zamion101 ve Teoman00 tarafından kodlanmaktayım... " +
                            "Benim gelişimimi takip etmek için abone olun! " +
                            "https://www.youtube.com/channel/UC8KdU8qxbhaPJSdYZrSAplA \n" + "Ayrıca ben açık kaynak kodlu olduğum için buradan" +
                            " nasıl kodlandığıma bakabilirsin... https://gitlab.com/TeonTR/Discord-Bot").queue();
                }else{
                    TMessage message = new TMessage(info,info.content);
                    CommandSearch.runCommand(info,message,info.content.split(" ")[0].substring(3).toLowerCase());
                }

                }



        }
    }

}
