package me.Zamion101.listener;

import me.Zamion101.manager.CommandSearch;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.role.update.GenericRoleUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class RoleChangeListener extends ListenerAdapter{


    @Override
    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent event) {
        ReadyListener.admins.clear();
        ReadyListener.searchAdmin(event);
    }

    @Override
    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {
        ReadyListener.admins.clear();
        ReadyListener.searchAdmin(event);
    }
}
