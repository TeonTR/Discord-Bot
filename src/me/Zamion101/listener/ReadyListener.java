package me.Zamion101.listener;

import me.Zamion101.Information;
import me.Zamion101.manager.CommandJson;
import me.Zamion101.manager.TRoles;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ReadyListener extends ListenerAdapter {

    public static Event event;
    public static List<String> admins = new ArrayList<>();
    public static String id = "393110517742567424";


    @Override
    public void onReady(ReadyEvent events) {
        event = events;
        for(Role r : events.getJDA().getGuildById(id).getRoles()){
            System.out.println("" + r.getName() + " /  " + r.getId());
        }
        searchAdmin(events);
    }

    public static void searchAdmin(Event event){
        Information information = new Information(event);

        for(Role role : event.getJDA().getGuildById(id).getRoles()){
            System.out.println(role.getName() + " / " + role.getId());
        }

        for(Member u : event.getJDA().getGuildById(id).getMembersWithRoles(TRoles.getRole(information,TRoles.TEON))){
                admins.add(u.getUser().getId());
        }
        for(Member u : event.getJDA().getGuildById(id).getMembersWithRoles(TRoles.getRole(information,TRoles.SULTAN))){
            admins.add(u.getUser().getId());
        }
        for(Member u : event.getJDA().getGuildById(id).getMembersWithRoles(TRoles.getRole(information,TRoles.VEZIR))){
            admins.add(u.getUser().getId());
        }

        System.out.println(admins);
        new CommandJson();



    }
}
