package me.Zamion101.listener;

import me.Zamion101.Information;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class JoinListener extends ListenerAdapter{

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        event.getUser().openPrivateChannel().complete().sendMessage("Sunucumuza Hoşgeldin " + event.getUser().getAsMention() + "\n"+
        " Burası TeonTR'nin discord sunucusudur. Bu sunucuda düzenin sağlanması ve ahlakın korunması için belli başlı kurallarımız bulunmaktadır.\n" +
                " Sunucuya katılarak bu kuralları kabul etmiş olursun ve kuralların çiğnenmesi otomatik olarak benim tarafımdan gözden geçirilip değerlendirilmektedir.\n" +
                " Bu yüzden benden hiçbirşey kaçmaz! Eğer kurallara uyarsan hiçbir sıkıntı çıkmaz ayrıca herhangi bir sorunda bana ulaşabilirsin `te!yardım`.\n" +
                " Unutmadan işte kurallarımız;\n" +
                " 1)-").queue();
        event.getGuild().getTextChannelById("395286185326542848").sendMessage("[Log]- {Yeni üyemiz var! " + event.getUser().getAsMention() + "}-").queue();
    }
}
