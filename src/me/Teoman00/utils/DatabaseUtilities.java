package me.Teoman00.utils;

import me.Zamion101.main.ZTeon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseUtilities {

    public static Connection con = ZTeon.con;

    public static void process(int id, int type) throws SQLException{

        PreparedStatement ps = con.prepareStatement("INSERT INTO msgchecker (islemid,typeid) VALUES (?,?)");
        ps.setInt(1, id);
        ps.setInt(2, type);
        ps.execute();
    }

    public static void addBadWord(String regex, int islemid, String kelime) throws SQLException{

        PreparedStatement ps = con.prepareStatement("INSERT INTO badwordlist (islemid,regex,kelime) VALUES (?,?,?)");
        ps.setInt(1, islemid);
        ps.setString(2, regex);
        ps.setString(3, kelime);
        ps.execute();

    }

    public static int getLastMsgCheckerID() throws SQLException{

        PreparedStatement ps = con.prepareStatement("SELECT * FROM msgchecker");
        ResultSet rs = ps.executeQuery();
        int size = 0;
        while(rs.next())
            size++;
        return size;

    }

    public static void initBadWords() throws SQLException {

        PreparedStatement ps = con.prepareStatement("SELECT regex FROM badwordlist");
        ResultSet rs = ps.executeQuery();
        int i = 1;
        while (rs.next()){
            ListUtilities.badWords.add(rs.getString(i));
            i++;
        }

    }

}
