package me.Teoman00.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ListUtilities {

    public static HashMap<String, String> replace = new HashMap<>();
    public static int lastMsgCheckerID = 0;
    public static ArrayList<String> badWords = new ArrayList<>();

    public static void init(){

        String[] replaceA = {"sa", "as", "nbr", "Teo", "Zami", "Teoya", "Zamiye", "ztn"};
        String[] replaceB = {"Selâmün Aleyküm", "Aleyküm Selâm", "Naber ?", "@Teoman00", "@Zamion101", "@Teoman00'a", "@Zamion101'e", "zaten"};
        for(int i = 0; i+1 < replaceA.length; i++){
            System.out.println("For i: " + i);
            replace.put(replaceA[i], replaceB[i]);
        }

        try {
            DatabaseUtilities.initBadWords();
            lastMsgCheckerID = DatabaseUtilities.getLastMsgCheckerID();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
