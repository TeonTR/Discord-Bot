package me.Teoman00;

import me.Teoman00.handlers.MessageChecker;
import me.Teoman00.listener.MessageListener;
import me.Teoman00.utils.ListUtilities;
import net.dv8tion.jda.core.JDABuilder;

import java.util.List;

public class TMain {

    public static void enable(JDABuilder jda){

        ListUtilities.init();
        jda.addEventListener(new MessageListener());


    }

}
