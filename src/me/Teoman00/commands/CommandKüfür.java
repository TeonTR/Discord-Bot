package me.Teoman00.commands;

import me.Teoman00.handlers.MessageChecker;
import me.Teoman00.utils.DatabaseUtilities;
import me.Teoman00.utils.ListUtilities;
import me.Zamion101.Information;
import me.Zamion101.TMessage;
import me.Zamion101.manager.TCommand;

import java.sql.PreparedStatement;

public class CommandKüfür extends TCommand{

    @Override
    public void init(String command, Class clazz, int permission) {
        super.init("küfür", this.getClass(), 2);
    }

    @Override
    public void run(Information info, TMessage message) {
        super.run(info, message);

        if(message.getArgs().length != 1) {
            info.messageChannel.sendMessage(info.user.getAsMention() + " Komutun kullanımı şu şekilde; te!küfür <kelime>").queue();
            return;
        }

        String küfür = message.getArgs()[0];
        MessageChecker mc = new MessageChecker();
        mc.addKüfür(küfür);
        if(mc.success)
            info.messageChannel.sendMessage(info.user.getAsMention() + " kelime başarıyla eklendi !").queue();
        else
            info.messageChannel.sendMessage(info.user.getAsMention() + " bir hata oluştu !").queue();


    }
}
