package me.Teoman00.handlers;

import me.Teoman00.utils.DatabaseUtilities;
import me.Teoman00.utils.ListUtilities;
import me.Zamion101.Information;

import java.sql.SQLException;

public class MessageChecker {

    private int id;
    //typeID = 1: Küfür Ekleme 2: Küfür Detect
    public boolean success;
    int resultID; //0: Bilinmeyen ! 1: Küfürlü 2: Reklamlı 3: Tehdit 4: Temiz


    public MessageChecker(){

        id = ListUtilities.lastMsgCheckerID ++;
    }

    public void addKüfür(String word){

        //(\b(?=\w*[sS])(?=\w*[eE])(?=\w*[lL])(?=\w*[aA])(?=\w*[mM]))([sS]{1,}|[eE]{1,}|[lL]{1,}|[aA]{1,}|[mM]{1,})

        try {
            DatabaseUtilities.process(id,1);
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        String regex = "(\\b";
        String splitOne = "(?=\\w*[x])";
        for(char c : word.toCharArray()){
            regex+=splitOne.replaceAll("x", String.valueOf(Character.toLowerCase(c)) + String.valueOf(Character.toUpperCase(c)));
        }
        regex += ")(";
        String splitTwo = "[x]{1,}";

        for(char c : word.toCharArray()){

            regex+=splitTwo.replaceAll("x", String.valueOf(Character.toLowerCase(c)) + String.valueOf(Character.toUpperCase(c)));
            regex+="|";
        }

        regex = regex.substring(0, regex.length()-1);
        regex += ")";

        try {
            DatabaseUtilities.addBadWord(regex,id,word);
        } catch (SQLException e) {
            success = false;
            e.printStackTrace();
            return;
        }
        ListUtilities.init();
        success = true;


    }

    public void küfürDetect(Information info){

        try {
            DatabaseUtilities.process(id,2);
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        info.message.delete().reason("Küfür algılandı").queue();
        info.user.openPrivateChannel().complete().sendMessage("Az önceki mesajınızda küfür algılanmıştır.\nEğer bunun bir yanlışlık sonucu olduğunu düşünüyorsanız bizimle iletişime geçebilirsiniz.").queue();

        /*Member m = info.guild.getMember(info.user);
        info.guild.getController().modifyMemberRoles(m, TRoles.getRole(info, TRoles.HALK), TRoles.getRole(info,TRoles.SUSTURULMUS)).queue();*/
        //TODO Mute sistemi gelecek.
    }

}
